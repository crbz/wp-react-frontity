const settings = {
  "name": "wp-react-frontity",
  "state": {
    "frontity": {
      "url": "https://destructo.ml/",
      "title": "JCDecaux",
      "description": "OportunosLatam"
    }
  },
  "packages": [
    {
      "name": "@frontity/twentytwenty-theme",
      "state": {
        "theme": {
          "menu": [
            [
              "Inicio",
              "/"
            ]
            ,
          
            [
              "Nosotros",
              "/category/nosotros/"
            ]
            ,
          
            [
              "Contacto",
              "/contacto/"
            ],
          
            [
              "Blog",
              "/blog/"
            ]
          ],
          "featured": {
            "showOnList": false,
            "showOnPost": false
          }
        }
      }
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "https://destructo.ml/"
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
